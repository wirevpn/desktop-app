#!/bin/bash

chmod a+x munkipkg

# build app
rm *.pkg
rm -rf build
mkdir build
cd build

git clone https://gitlab.com/wirevpn/daemon
cd daemon/macos
sh make.sh

cd ../../../

#./munkipkg --import build/daemon/macos/package/build/wirevpn-daemon-1.0.pkg package

#rm -rf package/Bom.txt package/build-info.plist
mkdir build/package
cp build-info.plist build/package/build-info.plist

mkdir -p build/package/payload/Applications
qtdeploy build darwin ../wirevpn
cp -R ../wirevpn/deploy/darwin/wirevpn.app build/package/payload/Applications/WireVPN.app

# build the package
./munkipkg build/package/

productbuild \
--distribution distribution.plist \
--resources ./Resources \
--package-path build/daemon/macos/package/build \
--package-path build/package/build \
WireVPN.pkg

productsign --sign "Developer ID Installer: Wirelabs OU" WireVPN.pkg WireVPN-signed.pkg