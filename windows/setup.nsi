OutFile "wirevpn-1.0-beta.exe"
RequestExecutionLevel admin
ManifestDPIAware true
SetCompressor /SOLID /FINAL lzma

!include LogicLib.nsh
!include x64.nsh
!include MUI2.nsh

Function .onInit
    # admin check
    UserInfo::GetAccountType
    pop $0
    ${If} $0 != "admin"
        MessageBox mb_iconstop "Administrator rights required!"
        SetErrorLevel 740
        Quit
    ${EndIf}

    # decide on install dir
    ${If} ${RunningX64}
        strcpy $INSTDIR $PROGRAMFILES64\WireVPN
    ${Else}
        strcpy $INSTDIR $PROGRAMFILES\WireVPN
    ${EndIf}

    # uninstall previous first
    ReadRegStr $R0 HKCU "Software\WireVPN" "installDir"
    StrCmp $R0 "" done

    MessageBox MB_OKCANCEL|MB_ICONEXCLAMATION \
    "WireVPN is already installed. $\n$\nClick 'OK' to remove the \
    previous version or 'Cancel' to cancel this upgrade." \
    IDOK uninst
    Abort

    # run the uninstaller
    uninst:
        ClearErrors
        Exec $R0\uninst.exe # install dir might be custom
    done:
FunctionEnd

Function createShortcuts
    # create start menu links
    CreateDirectory "$SMPROGRAMS\WireVPN"
    CreateShortCut "$SMPROGRAMS\WireVPN\WireVPN Client.lnk" "$INSTDIR\app\wirevpn.exe"
    CreateShortCut "$SMPROGRAMS\WireVPN\Uninstall WireVPN.lnk" "$INSTDIR\uninst.exe"

    # create desktop shortcut
    CreateShortCut "$DESKTOP\WireVPN Client.lnk" "$INSTDIR\app\wirevpn.exe"
FunctionEnd

Name "WireVPN"

!define MUI_ABORTWARNING
!define MUI_ICON "setup.ico"
!define MUI_UNICON "uninst.ico"
!define MUI_FINISHPAGE_SHOWREADME ""
!define MUI_FINISHPAGE_SHOWREADME_TEXT "Create shortcuts"
!define MUI_FINISHPAGE_SHOWREADME_FUNCTION createShortcuts

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "license.rtf"
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

!insertmacro MUI_LANGUAGE "English"

Section "WireVPN Client" SecClient
    # read only
    SectionIn RO
    SetOutPath $INSTDIR

    # files
    File "license.rtf"
    File "setup.ico"
    File /r "build\app"

    # registry key
    WriteRegStr HKCU "Software\WireVPN" "installDir" $INSTDIR

    # create uninstaller
    WriteUninstaller "$INSTDIR\uninst.exe"

    # register with add/remove
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\WireVPN" \
                "DisplayName" "WireVPN"
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\WireVPN" \
                "UninstallString" "$\"$INSTDIR\uninst.exe$\""
    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\WireVPN" \
                "DisplayIcon" "$\"$INSTDIR\setup.ico$\""
SectionEnd

Section "WireVPN Service" SecService
    # read only
    SectionIn RO
    SetOutPath $INSTDIR

    # files
    File /oname=wirevpn-daemon.exe "build\server.exe"

    # exec service
    Exec '"$INSTDIR\wirevpn-daemon.exe" install'
SectionEnd

Section "WireGuard Wintun Driver" SecDriver
    # read only
    SectionIn RO
    SetOutPath $INSTDIR

    # files
    File /r "wintun"

    # install it
    ${DisableX64FSRedirection}
    Exec '"$SYSDIR\PnPutil.exe" /add-driver "$INSTDIR\wintun\wintun.inf" /install'
    ${EnableX64FSRedirection}
SectionEnd

Section "Uninstall"
    # close app if running
    ${DisableX64FSRedirection}
    Exec '"$SYSDIR\taskkill.exe" /F /IM wirevpn.exe'
    ${EnableX64FSRedirection}

    # stop and remove service
    Exec '"$INSTDIR\wirevpn-daemon.exe" stop'
    Sleep 1000
    Exec '"$INSTDIR\wirevpn-daemon.exe" remove'
    Sleep 5000
    ${DisableX64FSRedirection}
    Exec '"$SYSDIR\taskkill.exe" /F /IM wirevpn-daemon.exe'
    ${EnableX64FSRedirection}

    # remove everything
    Delete "$INSTDIR\uninst.exe"
    Delete "$DESKTOP\WireVPN Client.lnk"
    RMDir /r "$INSTDIR"
    RMDir /r "$SMPROGRAMS\WireVPN"
    DeleteRegKey /ifempty HKCU "Software\WireVPN"
SectionEnd

!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
!insertmacro MUI_DESCRIPTION_TEXT ${SecClient} "GUI client for the WireVPN service"
!insertmacro MUI_DESCRIPTION_TEXT ${SecService} "WireVPN background service that manages the VPN"
!insertmacro MUI_DESCRIPTION_TEXT ${SecDriver} "WireGuard Windows driver"
!insertmacro MUI_FUNCTION_DESCRIPTION_END