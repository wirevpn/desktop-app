#!/bin/bash

rm -rf build
rm *.exe

mkdir build
cd build

git clone https://gitlab.com/wirevpn/daemon
cd daemon/windows
sh make.sh
cd ../../

mv daemon/windows/server.exe .

qtdeploy build windows ../../wirevpn
mv ../../wirevpn/deploy/windows app