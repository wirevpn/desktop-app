package main

import (
	"fmt"

	"github.com/therecipe/qt/widgets"
)

func cryptoErr(err error) string {
	return fmt.Sprintf("Can't generate crypto keys: %s", err)
}

func keychainErr(err error) string {
	return fmt.Sprintf("Can't access system keychain: %s", err)
}

func apiErr(err error) string {
	return fmt.Sprintf("%v", err)
}

func raiseCritical(msg string) {
	go mainThread.RunOnMain(func() {
		widgets.QMessageBox_Critical(nil, "OK", msg, widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
		mainApp.Quit()
	})
}

func raiseWarning(msg string) {
	go mainThread.RunOnMain(func() {
		widgets.QMessageBox_Warning(nil, "OK", msg, widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
	})
}

func raiseInfo(msg string) {
	go mainThread.RunOnMain(func() {
		widgets.QMessageBox_Information(nil, "OK", msg, widgets.QMessageBox__Ok, widgets.QMessageBox__Ok)
	})
}
