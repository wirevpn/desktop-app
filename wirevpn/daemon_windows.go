package main

import (
	"fmt"

	"gitlab.com/wirevpn/daemon/network"
)

func setupDaemon() daemonAPI {
	handlers := make(network.ClientHandlers)
	handlers[network.MsgConnect] = func(msg *network.Message) {
		resp := msg.Data.(bool)
		root.connectionStatus(resp)
		if !resp {
			root.raiseNotif("Can't establish connection")
		}
		root.connectionBusy(false)
	}
	handlers[network.MsgDisconnect] = func(msg *network.Message) {
		api.Disconnect()
		root.connectionStatus(false)
		root.connectionBusy(false)
	}
	handlers[network.MsgInterrupt] = func(msg *network.Message) {
		root.connectionStatus(false)
		raiseWarning("VPN connection dropped")
	}
	handlers[network.ConnClosed] = func(msg *network.Message) {
		root.connectionStatus(false)
		raiseCritical("Connection to WireVPN background service closed")
	}
	handlers[network.ConnError] = func(msg *network.Message) {
		raiseCritical("Can't connect to WireVPN background service. Please contact support.")
	}

	c, err := network.NewClient(handlers)
	if err != nil {
		raiseCritical(err.Error())
		return nil
	}

	if err := c.Establish(); err != nil {
		fmt.Println(err)
	} else {
		// Block until error
		go c.Start()
	}

	return c
}
