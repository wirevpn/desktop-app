package main

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/wirevpn/common/curve25519"
	"gitlab.com/wirevpn/desktop-app/wirevpn/libs/settings"
)

func setupSettings() *settings.Settings {
	stn, err := settings.NewSettings("WireVPN")
	if err != nil {
		raiseCritical(err.Error())
		return nil
	}

	if stn.DeviceID() == "" {
		u2, err := uuid.NewV4()
		if err != nil {
			raiseCritical(err.Error())
			return nil
		}
		stn.SetDeviceID(u2.String())
	}

	if stn.LastCountry() == "" {
		stn.SetLastCountry("DE")
	}

	if priv, pub := stn.Keys(); priv == "" || pub == "" {
		pr, pu, err := curve25519.GenerateKeys()
		if err != nil {
			raiseCritical(err.Error())
			return nil
		}
		stn.SetKeyPair(pr, pu)
		privKey = pr
	} else {
		privKey = priv
	}

	api.SetToken(stn.Token())

	return stn
}
