package main

import (
	"github.com/therecipe/qt/core"
)

type qtobj struct {
	obj *core.QObject
}

func newQTObj(object *core.QObject) *qtobj {
	return &qtobj{obj: object}
}

func (q *qtobj) setProperty(property string, value *core.QVariant) {
	mainThread.RunOnMain(func() { q.obj.SetProperty(property, value) })
}

func (q *qtobj) raiseNotif(msg string) {
	q.setProperty("errorPopupText", core.NewQVariant14(msg))
}

func (q *qtobj) connectionStatus(state bool) {
	q.setProperty("connectionStatus", core.NewQVariant11(state))
}

func (q *qtobj) connectionBusy(state bool) {
	q.setProperty("connectBusy", core.NewQVariant11(state))
}
