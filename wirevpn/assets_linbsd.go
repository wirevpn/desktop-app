// +build linux freebsd openbsd

package main

import (
	"path/filepath"
)

func setupAssets(exePath string) {
	imgWinIcon = filepath.Join(exePath, "images", "winIcon.png")
	imgSysIcon = filepath.Join(exePath, "images", "sysIcon.png")
	imgSysIconGreen = filepath.Join(exePath, "images", "sysIconGreen.png")
	imgQrPlaceholder = filepath.Join(exePath, "images", "ph.png")
	fontInconsolata = filepath.Join(exePath, "fonts", "Inconsolata.otf")
	fontOpenSansBold = filepath.Join(exePath, "fonts", "OpenSans-Bold.ttf")
	fontOpenSans = filepath.Join(exePath, "fonts", "OpenSans-Regular.ttf")
}
