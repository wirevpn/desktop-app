package main

import (
	"path/filepath"
)

func setupAssets(exePath string) {
	imgWinIcon = filepath.Join(exePath, "..", "Resources", "winIcon.icns")
	imgSysIcon = filepath.Join(exePath, "..", "Resources", "sysIcon.png")
	imgSysIconGreen = filepath.Join(exePath, "..", "Resources", "sysIconGreen.png")
	imgQrPlaceholder = filepath.Join(exePath, "..", "Resources", "ph.png")
	fontInconsolata = filepath.Join(exePath, "..", "Resources", "Inconsolata.otf")
	fontOpenSansBold = filepath.Join(exePath, "..", "Resources", "OpenSans-Bold.ttf")
	fontOpenSans = filepath.Join(exePath, "..", "Resources", "OpenSans-Regular.ttf")
}
