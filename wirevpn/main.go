package main

import (
	"os"
	"path/filepath"
	"runtime"

	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/wirevpn/desktop-app/wirevpn/libs/settings"
	"gitlab.com/wirevpn/desktop-app/wirevpn/libs/wirevpn"
)

func init() {
	runtime.LockOSThread()
	customWindow_QmlRegisterType2("CustomQmlTypes", 1, 0, "CustomWindow")
	listItem_QRegisterMetaType()
}

type mainThreadHelper struct {
	core.QObject
	_ func(f func()) `signal:"runOnMain,auto`
}

func (*mainThreadHelper) runOnMain(f func()) { f() }

const (
	winWidth       = 680
	winHeight      = 590
	winSpacing     = 20
	desktopSpacing = 30
	apiServer      = "https://app.wirevpn.net/v3"
)

var (
	stn         *settings.Settings
	mainApp     *widgets.QApplication
	countryList *customListModel
	root        *qtobj
	service     daemonAPI
	privKey     string
	exePath     string
	api         = wirevpn.NewClient(apiServer)
	mainThread  = NewMainThreadHelper(nil)
)

func main() {
	var err error
	exePath, err = os.Executable()
	if err != nil {
		raiseCritical("Can't resolve executable path")
		os.Exit(0)
	}
	exePath = filepath.Dir(exePath)

	component := setupQT()
	service = setupDaemon()
	stn = setupSettings()
	root = newQTObj(component.Create(nil))
	mainApp.Exec()
}
