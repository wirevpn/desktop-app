import QtQuick 2.12
import QtQuick.Controls 2.12
import CustomQmlTypes 1.0
import QtQuick.Window 2.2

CustomWindow {
    property var countryCode: "auto"
    signal connectSignal(string arg)

    id: root
    objectName: "root"
    flags: Qt.WA_TranslucentBackground | Qt.FramelessWindowHint  | Qt.NoDropShadowWindowHint
    title: "WireVPN"
    color: "#00000000"
    visible: false

    Component.onCompleted: {
        setX(Screen.width / 2 - 340)
        setY(Screen.height / 2 - 295)
        visible = true
    }

    StateGroup {
        id: screens
        state: loginStatus ? "dashScreen" : "loginScreen"

        states: [
            State {
                name: "loginScreen"
                PropertyChanges { target: loginScreen; opacity: 1; enabled: true }
                PropertyChanges { target: dashScreen; opacity: 0; enabled: false }
            },
            State {
                name: "dashScreen"
                PropertyChanges { target: loginScreen; opacity: 0; enabled: false }
                PropertyChanges { target: dashScreen; opacity: 1; enabled: true }
            }
        ]

        transitions: Transition {
                reversible: true
                from: "loginScreen"
                to: "dashScreen"
                NumberAnimation { targets: [loginScreen, dashScreen]; property: "opacity"; duration: 250 }
            }
    }

    onConnectionStatusChanged: {
        if(connectionStatus) {
            connectButton.state = "state2"
            setOnlineStatus()
            connectSignal(countryCode)
        } else {
            connectButton.state = "state1"
            setOfflineStatus()
            connectSignal("")
        }
        flickable.enabled = true
    }

    onLoginStatusChanged: {
        screens.state = loginStatus ? "dashScreen" : "loginScreen"
        connectBusy = false
        loginBusy = false
    }

    onAccountPassChanged: {
        accountPassInput.text = accountPass
        accountPassLabel.text = accountPass
        qrcode.source = "image://qrcode/" + accountPass
    }

    onErrorPopupTextChanged: {
        if (errorPopupText != "") {
            errorText.text = errorPopupText
            errorPopup.visible = true
        }
        flickable.enabled = true
    }

    // Drop shadow
    Image {
        source: "images/drop.png"

        anchors {
            fill: parent
        }
    }

    // Login screen
    Item {
        id: loginScreen

        anchors {
            fill: parent
        }

        Image {
            source: bgImg

            anchors {
                fill: parent
                margins: 20
            }

            Image {
                source: "images/logo.png"
                x: 30
                y: 30
                width: 100
                fillMode: Image.PreserveAspectFit
            }

            // Close button
            Image {
                width: 20
                fillMode: Image.PreserveAspectFit
                source: "images/close2.png"

                anchors {
                    top: parent.top
                    right: parent.right
                    topMargin: 10
                    rightMargin: 10
                }

                MouseArea {
                    cursorShape: containsMouse ? Qt.PointingHandCursor : Qt.ArrowCursor
                    hoverEnabled: true

                    anchors {
                        fill: parent
                    }

                    onClicked: {
                        root.toggleVisibility(false)
                    }
                }
            }

            CustomBusy {
                anchors {
                    centerIn: parent
                    verticalCenterOffset: 80
                }

                running: root.loginBusy
            }

            CustomButton {
                width: 220
                height: 44
                radius: 22
                colorState1: "#000000ff"
                hoverState1: "#000000ff"
                textState1: "Sign In"
                textColor: "#e5e2e1"
                onClicked: {
                    root.login(accountPassInput.text)
                }

                anchors {
                    centerIn: parent
                    verticalCenterOffset: 160
                }

                border {
                    color: "#e5e2e1"
                    width: 2
                }
            }

            CustomButton {
                width: 220
                height: 44
                radius: 22
                colorState1: "#e5e2e1"
                hoverState1: "#cccccc"
                textState1: "New Account"
                textColor: "#4b6477"
                onClicked: {
                    Qt.openUrlExternally("https://www.wirevpn.com/login?pass=");
                }

                anchors {
                    centerIn: parent
                    verticalCenterOffset: 225
                }
            }

            CustomInput {
                id: accountPassInput
                width: 380
                height: 50
                textSize: 26
                textColor: "#e5e2e1"
                placeholder: "A4GHE345LL3K"
                text: root.accountPass

                anchors {
                    centerIn: parent
                    verticalCenterOffset: -80
                }
            }

            Text {
                color: "#e5e2e1"
                text: "Account Pass"
                x: accountPassInput.x
                y: accountPassInput.y + accountPassInput.height + 10

                font {
                    family: "Open Sans"
                    pixelSize: 14
                    capitalization: Font.AllUppercase
                }
            }
        }
    }

    // Dashboard screen
    Item {
        id: dashScreen

        anchors {
            fill: parent
        }

        // Background
        Item {
            width: 640
            height: 550

            anchors {
                centerIn: parent
            }

            Rectangle {
                color: "#e5e2e1"
                radius: 16

                anchors {
                    fill: parent
                    leftMargin: spacing
                }
            }

            // Close button
            Image {
                width: 20
                fillMode: Image.PreserveAspectFit
                source: "images/close.png"

                anchors {
                    top: parent.top
                    right: parent.right
                    topMargin: 10
                    rightMargin: 10
                }

                MouseArea {
                    cursorShape: containsMouse ? Qt.PointingHandCursor : Qt.ArrowCursor
                    hoverEnabled: true

                    anchors {
                        fill: parent
                    }

                    onClicked: {
                        root.toggleVisibility(false)
                    }
                }
            }

            // Right side country list
            Item {
                anchors {
                    fill: parent
                    leftMargin: parent.width / 2
                    topMargin: 90
                    bottomMargin: 120
                }

                Component {
                    id: countryItem

                    Item {
                        Connections{
                            target: root
                            onConnectSignal: {
                                selected.color = code == arg ? "#15000000" : "#00000000"
                            }
                        }

                        height: 50

                        anchors {
                            left: parent.left
                            right: parent.right
                        }

                        Image {
                            anchors {
                                verticalCenter: parent.verticalCenter
                                left: parent.left
                                leftMargin: 30
                            }
                            id: flagImg
                            source: flag
                            fillMode: Image.PreserveAspectFit
                            height: 32
                        }

                        Text {
                            anchors {
                                bottom: flagImg.bottom
                                left: flagImg.right
                                leftMargin: 20
                                bottomMargin: 3
                            }
                            text: name
                            color: "#4b6477"
                            font {
                                family: "Open Sans"
                                pixelSize: 14
                            }
                        }

                        Image {
                            anchors{
                                bottom: flagImg.bottom
                                right: parent.right
                                rightMargin: 30
                            }
                            source: strength
                            fillMode: Image.PreserveAspectFit
                            height: 32
                        }

                        Rectangle {
                            anchors {
                                left: parent.left
                                right: parent.right
                                bottom: parent.bottom
                            }
                            height: 1
                            color: "#15000000"
                        }

                        Rectangle {
                            anchors {
                                fill: parent
                            }
                            id: countryHover
                            color: "#00000000"
                        }

                        Rectangle {
                            anchors {
                                fill: parent
                            }
                            id: selected
                            color: "#00000000"
                        }

                        MouseArea {
                            anchors {
                                fill: parent
                            }

                            hoverEnabled: true

                            onClicked: {
                                flickable.currentIndex = index
                            }

                            onEntered: {
                                if (index != flickable.currentIndex) {
                                    countryHover.color = "#05000000"
                                }
                            }

                            onExited: { countryHover.color = "#00000000"}
                        }
                    }
                }

                ListView {
                    id: flickable
                    clip: true
                    model: CustomListModel
                    delegate: countryItem
                    highlight: Rectangle { color: "#11000000" }

                    anchors {
                        fill: parent
                    }

                    ScrollBar.vertical: ScrollBar {
                        parent: flickable.parent

                        anchors {
                            top: flickable.top
                            left: flickable.right
                            bottom: flickable.bottom
                        }

                        policy: ScrollBar.AlwaysOn
                    }

                    onCurrentIndexChanged: {
                        root.countryCode = model.getCurrentCode(currentIndex)
                    }
                }
            }

            // Right side
            Item {
                anchors {
                    fill: parent
                    rightMargin: 30
                    leftMargin: parent.width / 2 + 30
                    topMargin: 40
                    bottomMargin: 40
                }

                // Title
                Text {
                    id: countryTitle
                    text: "Choose Location"
                    color: "#4b6477"

                    anchors {
                        left: parent.left
                    }

                    font {
                        family: "Open Sans"
                        pixelSize: 24
                        bold: true
                    }
                }

                // Connect button
                CustomButton {
                    id: connectButton
                    width: 250
                    height: 50
                    radius: 10
                    colorState1: "#365b9d"
                    hoverState1: "#517bc3"
                    colorState2: "#99c99e"
                    hoverState2: "#d2bc5d"
                    textState1: "Connect"
                    textState2: "Disconnect"
                    textColor: "#e5e2e1"
                    textBold: true
                    state: connectionStatus ? "state2" : "state1"
                    enable: !root.connectBusy

                    onClicked: {
                        flickable.enabled = false
                        if (!root.connectionStatus)
                            root.connect(root.countryCode)
                        else
                            root.disconnect()
                    }

                    anchors {
                        horizontalCenter: parent.horizontalCenter
                        bottom: parent.bottom
                    }

                    CustomBusy {
                        anchors {
                            verticalCenter: parent.verticalCenter
                            left: connectButton.left
                            leftMargin: 10
                        }

                        running: root.connectBusy
                    }
                }
            }
        }

        // Left side
        Item {
            anchors {
                fill: parent
                leftMargin: spacing
                bottomMargin: spacing
                topMargin: spacing
                rightMargin: parent.width / 2
            }

            Image {
                source: "images/halfBg.png"

                anchors {
                    fill: parent
                }

                Image {
                    id: qrcode
                    y: 40
                    width: 200
                    height: 200
                    source: "image://qrcode/" + root.accountPass
                    smooth: false

                    anchors {
                        horizontalCenter: parent.horizontalCenter
                    }

                    MouseArea {
                        cursorShape: containsMouse ? Qt.PointingHandCursor : Qt.ArrowCursor
                        hoverEnabled: true

                        anchors {
                            fill: parent
                        }

                        onClicked: {
                            Qt.openUrlExternally("https://www.wirevpn.com/login?pass="+root.accountPass);
                        }
                    }
                }

                CustomInput {
                    id: accountPassLabel
                    y: qrcode.y + qrcode.height + 30
                    width: 260
                    height: 50
                    readOnly: true
                    text: root.accountPass
                    textSize: 28
                    textColor: "#e5e2e1"

                    anchors {
                        horizontalCenter: parent.horizontalCenter
                    }
                }

                Text {
                    color: "#e5e2e1"
                    text: "Account Pass"
                    x: accountPassLabel.x
                    y: accountPassLabel.y + accountPassLabel.height + 10

                    font {
                        family: "Open Sans"
                        pixelSize: 14
                        capitalization: Font.AllUppercase
                    }
                }

                Rectangle {
                    width: 260
                    height: 60
                    color: "#00000000"
                    radius: 16
                    visible: notification != ""

                    MouseArea {
                        cursorShape: containsMouse ? Qt.PointingHandCursor : Qt.ArrowCursor
                        hoverEnabled: true

                        anchors {
                            fill: parent
                        }

                        onClicked: {
                            Qt.openUrlExternally("https://www.wirevpn.com/login?pass="+root.accountPass);
                        }
                    }

                    anchors {
                        horizontalCenter: parent.horizontalCenter
                        bottom: parent.bottom
                        bottomMargin: 40
                    }

                    border {
                        color: "#e5e2e1"
                        width: 1
                    }

                    Image {
                        width: 24
                        height: 24
                        source: "images/attention.png"

                        anchors {
                            verticalCenter: parent.verticalCenter
                            left: parent.left
                            leftMargin: 20
                        }
                    }

                    Text {
                        id: notif
                        color: "#e5e2e1"
                        width: 180
                        wrapMode: Text.WordWrap
                        text: notification

                        anchors {
                            verticalCenter: parent.verticalCenter
                            left: parent.left
                            leftMargin: 64
                        }

                        font {
                            family: "Open Sans"
                            pixelSize: 10
                        }
                    }
                }
            }
        }
    }

    // Error popup
    Image {
        id: errorPopup
        source: "images/error.png"
        width: 640
        height: 111
        y: 20
        visible: false

        anchors {
            horizontalCenter: parent.horizontalCenter
        }

        Item {
            anchors {
                fill: parent
                bottomMargin: 16
                leftMargin: 20
            }

            Text {
                id: errorTitle
                color: "#e5e2e1"
                text: "Error"

                anchors {
                    verticalCenter: parent.verticalCenter
                    verticalCenterOffset: -24
                }

                font {
                    bold: true
                    family: "Open Sans"
                    pixelSize: 14
                }
            }

            Text {
                id: errorText
                width: 500
                color: "#e5e2e1"
                wrapMode: Text.WordWrap
                text: ""

                anchors {
                    top: errorTitle.bottom
                    topMargin: 5
                }

                font {
                    family: "Open Sans"
                    pixelSize: 12
                }
            }

            // Close button
            Image {
                width: 20
                fillMode: Image.PreserveAspectFit
                source: "images/close3.png"

                anchors {
                    verticalCenter: parent.verticalCenter
                    right: parent.right
                    rightMargin: 20
                }

                MouseArea {
                    cursorShape: containsMouse ? Qt.PointingHandCursor : Qt.ArrowCursor
                    hoverEnabled: true

                    anchors {
                        fill: parent
                    }

                    onClicked: {
                        errorPopupText = ""
                        errorPopup.visible = false
                    }
                }
            }
        }
    }

    // Title dragger
    MouseArea {
        property variant clickPos: "1,1"

        width: 550
        height: 30
        x: 65
        y: 20

        onPressed: {
            clickPos = Qt.point(mouse.x,mouse.y)
        }

        onPositionChanged: {
            var delta = Qt.point(mouse.x-clickPos.x, mouse.y-clickPos.y)
            root.x = root.x + delta.x
            root.y = root.y + delta.y
        }
    }
}