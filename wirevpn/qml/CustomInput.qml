import QtQuick 2.12
import QtQuick.Controls 2.12

Rectangle {
    property alias textColor: textField.color
    property alias textBold: textField.font.bold
    property alias textSize: textField.font.pixelSize
    property alias borderWidth: bottomBorder.height
    property alias borderColor: bottomBorder.color
    property alias borderVisible: bottomBorder.visible
    property alias placeholder: placeholder.text
    property alias text: textField.text
    property alias readOnly: textField.readOnly
    property alias placeholderVisible: placeholder.visible

    onTextChanged: {
        placeholderVisible = false
    }

    color: "#00000000"

    Text {
        id: placeholder
        color: "#ffffff"
        opacity: 0.2
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter

        anchors {
            fill: parent
            bottomMargin: bottomBorder.height
        }

        font {
            bold: textField.font.bold
            family: textField.font.family
            pixelSize: textField.font.pixelSize
            capitalization: textField.font.capitalization
        }
    }

    Menu {
        id: contextMenu
        MenuItem {
            text: "&Copy";
            enabled: textField.selectedText
            onTriggered: textField.copy()
        }
        MenuItem{
            text: "&Paste";
            enabled: textField.canPaste
            onTriggered: textField.paste()
        }
    }

    TextField {
        id: textField
        background: Item { opacity: 0 }
        selectByMouse: true
        maximumLength: 12
        horizontalAlignment: TextInput.AlignHCenter
        verticalAlignment: TextInput.AlignVCenter
        selectionColor: "#ff5555"
        persistentSelection: true
        focus: true

        onTextEdited: {
            placeholder.visible = false
        }

        onReleased: {
            if (event.button == Qt.RightButton) {
                contextMenu.x = x + event.x
                contextMenu.y = y + event.y
                contextMenu.visible = true
            }
        }

        onPressed: {
            if (event.button == Qt.LeftButton) {
                contextMenu.visible = false
            }
        }

        anchors {
            fill: parent
            bottomMargin: bottomBorder.height
        }

        font {
            bold: false
            family: "Inconsolata"
            pixelSize: 28
            capitalization: Font.AllUppercase
        }
    }

    Rectangle {
        id: bottomBorder
        color: parent.textColor
        x: 0
        y: parent.height - height
        width: parent.width
        height: 2
    }
}