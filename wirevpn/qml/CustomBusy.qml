import QtQuick 2.12
import QtQuick.Controls 2.12

BusyIndicator {
    id: control

    contentItem: Item {
        implicitWidth: 32
        implicitHeight: 32

        Item {
            id: item
            x: parent.width / 2 - 16
            y: parent.height / 2 - 16
            width: 32
            height: 32
            opacity: control.running ? 1 : 0

            Behavior on opacity {
                OpacityAnimator {
                    duration: 250
                }
            }

            RotationAnimator {
                target: item
                running: control.visible && control.running
                from: 0
                to: 360
                loops: Animation.Infinite
                duration: 1250
            }

            Repeater {
                id: repeater
                model: 6

                Rectangle {
                    x: item.width / 2 - width / 2
                    y: item.height / 2 - height / 2
                    implicitWidth: 6
                    implicitHeight: 6
                    radius: 5
                    color: "#ffffff"
                    transform: [
                        Translate {
                            y: -Math.min(item.width, item.height) * 0.5 + 5
                        },
                        Rotation {
                            angle: index / repeater.count * 360
                            origin.x: 3
                            origin.y: 3
                        }
                    ]
                }
            }
        }
    }
}