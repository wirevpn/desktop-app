import QtQuick 2.12

Rectangle {
    property color colorState1: "#ffffff"
    property color hoverState1: "#dddddd"
    property string textState1: "state1"
    property color colorState2: "#ffffff"
    property color hoverState2: "#dddddd"
    property string textState2: "state2"
    property alias text: label.text
    property alias textColor: label.color
    property alias textBold: label.font.bold
    property alias textSize: label.font.pixelSize
    property alias enable: mouseArea.enabled

    signal clicked()

    id: button
    smooth: true;
    radius: 30
    state: "state1"
    states: [
        State {
            name: "state1"
            PropertyChanges {
                target: button
                color: colorState1
                text: textState1
            }
        },
        State {
            name: "hover1"
            PropertyChanges {
                target: button
                color: hoverState1
                text: textState1
            }
        },
        State {
            name: "state2"
            PropertyChanges {
                target: button
                color: colorState2
                text: textState2
            }
        },
        State {
            name: "hover2"
            PropertyChanges {
                target: button
                color: hoverState2
                text: textState2
            }
        }
    ]

    Text {
        id: label
        text: "Button"
        color: "#000000"

        anchors {
            centerIn: parent
            verticalCenterOffset: 1
        }

        font {
            pixelSize: 14
            family: "Open Sans"
            bold: false
            capitalization: Font.AllUppercase
        }
    }

    MouseArea {
        id: mouseArea

        cursorShape: containsMouse ? Qt.PointingHandCursor : Qt.ArrowCursor
        hoverEnabled: true
        onEntered: {
            if (button.state == "state1")
                button.state = "hover1"
            else if (button.state == "state2")
                button.state = "hover2"
        }
        onExited: {
            if (button.state == "hover1")
                button.state = "state1"
            else if (button.state == "hover2")
                button.state = "state2"
        }
        onClicked:{
            button.clicked()
            if (containsMouse) {
                if (button.state == "state1")
                    button.state = "hover1"
                else if (button.state == "state2")
                    button.state = "hover2"
            }
        }
        anchors {
            fill: parent
        }
    }
}