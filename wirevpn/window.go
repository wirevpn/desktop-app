package main

import (
	"fmt"
	"math/rand"
	"runtime"
	"strings"
	"time"

	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/quick"
	"github.com/therecipe/qt/widgets"
	"gitlab.com/wirevpn/common/config"
	"gitlab.com/wirevpn/common/curve25519"
	"gitlab.com/wirevpn/desktop-app/wirevpn/libs/wirevpn"
)

type customWindow struct {
	quick.QQuickWindow

	isVisible bool

	sysIconData         *gui.QIcon
	sysIconGreenData    *gui.QIcon
	sysIcon             *widgets.QSystemTrayIcon
	sysMenu             *widgets.QMenu
	sysDisconnectAction *widgets.QAction
	sysToggleAction     *widgets.QAction
	sysConnect          *widgets.QMenu
	sysConnectAction    *widgets.QAction

	_ float64                  `property:"spacing"`
	_ string                   `property:"notification"`
	_ string                   `property:"bgImg"`
	_ string                   `property:"accountPass"`
	_ string                   `property:"errorPopupText"`
	_ bool                     `property:"loginStatus"`
	_ bool                     `property:"connectionStatus"`
	_ bool                     `property:"connectBusy"`
	_ bool                     `property:"loginBusy"`
	_ func()                   `constructor:"init"`
	_ func(bool)               `signal:"toggleVisibility,auto"`
	_ func(accountPass string) `signal:"login,auto"`
	_ func(countryCode string) `signal:"connect,auto"`
	_ func()                   `signal:"disconnect,auto"`
	_ func()                   `signal:"setOnlineStatus,auto"`
	_ func()                   `signal:"setOfflineStatus,auto"`
}

func (c *customWindow) init() {
	// System tray menu settings
	sysShowTrigger := func() {
		if !c.isVisible {
			// Set window pos
			func() {
				var (
					globalMousePos = gui.QCursor_Pos()
					screen         = mainApp.ScreenAt(globalMousePos)
					hw             = screen.AvailableSize().Width() / 2
					hh             = screen.AvailableSize().Height() / 2
					topLeftX       = screen.Geometry().TopLeft().X()
					topLeftY       = screen.Geometry().TopLeft().Y()
					mousePosX      = globalMousePos.X() - topLeftX
					mousePosY      = globalMousePos.Y() - topLeftY
				)
				// Find the corner
				if mousePosX < hw && mousePosY < hh {
					// Top left
					c.SetPosition2(desktopSpacing+topLeftX, desktopSpacing+topLeftY)
				} else if mousePosX >= hw && mousePosY < hh {
					// Top right
					c.SetPosition2(hw*2+topLeftX-desktopSpacing-winWidth, desktopSpacing+topLeftY)
				} else if mousePosX >= hw && mousePosY >= hh {
					// Bottom right
					c.SetPosition2(hw*2+topLeftX-desktopSpacing-winWidth, hh*2+topLeftY-desktopSpacing-winHeight)
				} else {
					// Bottom left
					c.SetPosition2(desktopSpacing+topLeftX, hh*2+topLeftY-desktopSpacing-winHeight)
				}
			}()
		}
		c.toggleVisibility(!c.isVisible)
	}
	c.sysMenu = widgets.NewQMenu(nil)
	c.sysConnect = widgets.NewQMenu2("Connect", nil)
	c.sysConnectAction = c.sysMenu.AddMenu(c.sysConnect)
	c.sysDisconnectAction = c.sysMenu.AddAction("Disconnect")
	c.sysDisconnectAction.SetVisible(false)
	c.sysDisconnectAction.ConnectTriggered(func(checked bool) { c.disconnect() })
	c.sysMenu.AddSeparator()
	c.sysToggleAction = c.sysMenu.AddAction("Hide WireVPN")
	c.sysToggleAction.ConnectTriggered(func(checked bool) {
		sysShowTrigger()
	})
	c.sysMenu.AddAction("Quit").ConnectTriggered(func(checked bool) {
		c.sysIcon.SetVisible(false)
		mainApp.Quit()
	})

	// System tray icon settings
	c.sysIconData = gui.NewQIcon5(imgSysIcon)
	c.sysIconGreenData = gui.NewQIcon5(imgSysIconGreen)
	c.sysIcon = widgets.NewQSystemTrayIcon2(c.sysIconData, c)
	c.sysIcon.SetVisible(true)
	c.sysIcon.SetContextMenu(c.sysMenu)
	if runtime.GOOS == "windows" {
		c.sysIcon.ConnectActivated(func(reason widgets.QSystemTrayIcon__ActivationReason) {
			if reason == widgets.QSystemTrayIcon__Trigger {
				sysShowTrigger()
			}
		})
	}

	// Window settings
	c.SetMinimumSize(core.NewQSize2(winWidth, winHeight))
	c.SetTextRenderType(quick.QQuickWindow__NativeTextRendering)
	c.SetSpacing(float64(winSpacing))
	c.SetConnectionStatus(false)
	c.SetConnectBusy(false)
	c.SetLoginBusy(false)
	c.SetAccountPass(c.getAccountPass())
	c.SetBgImg(c.getRandomBg())
	if c.isLoggedIn() {
		c.refresh()
		c.SetLoginStatus(true)
	} else {
		c.SetLoginStatus(false)
	}
	c.SetScreen(mainApp.PrimaryScreen())
	c.isVisible = true

	if runtime.GOOS == "darwin" {
		mainApp.ConnectApplicationStateChanged(func(state core.Qt__ApplicationState) {
			if mainApp.ApplicationState() == core.Qt__ApplicationActive {
				c.toggleVisibility(true)
			}
		})
	}
}

func (c *customWindow) raiseNotif(msg string) {
	c.SetErrorPopupText(msg)
}

// [QML] Toggles window's visibility
func (c *customWindow) toggleVisibility(state bool) {
	if c.isVisible && !state {
		c.Lower()
		c.Hide()
		c.isVisible = false
		c.sysToggleAction.SetText("Show WireVPN")
		return
	}
	if !c.isVisible && state {
		c.Show()
		c.Raise()
		c.RequestActivate()
		c.isVisible = true
		c.sysToggleAction.SetText("Hide WireVPN")
	}
}

// [QML] Change UI properties to online
func (c *customWindow) setOnlineStatus() {
	c.sysConnectAction.SetVisible(false)
	c.sysDisconnectAction.SetVisible(true)
	c.sysIcon.SetIcon(c.sysIconGreenData)
}

// [QML] Change UI properties to offline
func (c *customWindow) setOfflineStatus() {
	c.sysConnectAction.SetVisible(true)
	c.sysDisconnectAction.SetVisible(false)
	c.sysIcon.SetIcon(c.sysIconData)
}

// [API] Refresh all info
func (c *customWindow) refresh() {
	c.setCountryList()
	c.setNotification()
}

// [API] Returns notifications from server
func (c *customWindow) setNotification() {
	go func() {
		u, err := api.UserInfo()
		if err != nil {
			if _, ok := err.(*wirevpn.AuthErr); ok {
				mainThread.RunOnMain(func() {
					c.SetLoginStatus(false)
					c.raiseNotif(err.Error())
				})
				return
			}
			mainThread.RunOnMain(func() { c.raiseNotif(apiErr(err)) })
			return
		}

		var ret string
		if u.DaysLeft <= 7 {
			if u.DaysLeft <= 1 {
				ret = fmt.Sprintf("You have %d day left\n", u.DaysLeft)
			} else {
				ret = fmt.Sprintf("You have %d days left\n", u.DaysLeft)
			}
			if u.Plan == "" {
				ret = ret + "Please subscribe to avoid service interruption"
			} else {
				ret = ret + "You will be charged automatically"
			}
		}

		if u.ClientVer != version {
			raiseInfo("A new version of the client exists. Please upgrade at https://www.wirevpn.com")
		}

		mainThread.RunOnMain(func() { c.SetNotification(ret) })
	}()
}

// [API] Returns the active country list
func (c *customWindow) setCountryList() {
	go func() {
		countries, err := api.CountryList()
		if err != nil {
			if _, ok := err.(*wirevpn.AuthErr); ok {
				mainThread.RunOnMain(func() {
					c.SetLoginStatus(false)
					c.raiseNotif(err.Error())
				})
				return
			}
			mainThread.RunOnMain(func() { c.raiseNotif(apiErr(err)) })
		}

		// Reset sysConnect context menu
		c.sysConnect.Clear()

		var items []*listItem
		// Add auto to top
		auto := NewListItem(nil)
		auto.SetCode("auto")
		auto.SetFlag("images/auto.png")
		auto.SetName("Auto Select Country")
		auto.SetStrength("images/signal5.png")
		items = append(items, auto)

		for _, v := range countries {
			ccode := strings.ToLower(v.Code)
			cname := v.Name
			it := NewListItem(nil)
			it.SetCode(ccode)
			it.SetFlag(fmt.Sprintf("images/%s.png", ccode))
			it.SetName(cname)
			it.SetStrength(fmt.Sprintf("images/signal%d.png", v.Strength))
			items = append(items, it)

			// Populate context menu
			mainThread.RunOnMain(func() {
				c.sysConnect.AddAction(cname).ConnectTriggered(func(checked bool) {
					c.Connect(ccode)
				})
			})
		}

		mainThread.RunOnMain(func() {
			countryList.BeginResetModel()
			countryList.SetItems(items)
			countryList.EndResetModel()
		})
	}()
}

// [API] [QML] Logs user in with given account pass
func (c *customWindow) login(accountPass string) {
	c.SetLoginBusy(true)

	go func() {
		defer c.SetLoginBusy(false)

		accountPass = strings.ToUpper(accountPass)

		priv, pub, err := curve25519.GenerateKeys()
		if err != nil {
			mainThread.RunOnMain(func() { c.raiseNotif(cryptoErr(err)) })
			return
		}

		err = stn.SetKeyPair(priv, pub)
		if err != nil {
			mainThread.RunOnMain(func() { c.raiseNotif(keychainErr(err)) })
			return
		}

		token, err := api.Login(accountPass, stn.DeviceID(), pub)
		if err != nil {
			mainThread.RunOnMain(func() { c.raiseNotif(apiErr(err)) })
			return
		}
		api.SetToken(token)

		err = stn.SetToken(token)
		if err != nil {
			mainThread.RunOnMain(func() { c.raiseNotif(keychainErr(err)) })
			return
		}

		err = stn.SetAccountPass(accountPass)
		if err != nil {
			mainThread.RunOnMain(func() { c.raiseNotif(keychainErr(err)) })
			return
		}

		mainThread.RunOnMain(func() {
			c.refresh()
			c.SetAccountPass(c.getAccountPass())
			c.SetLoginStatus(true)
		})
	}()
}

// [API] [QML] Get country config and connect to VPN
func (c *customWindow) connect(countryCode string) {
	c.SetConnectBusy(true)

	go func() {
		quick, err := api.Connect(countryCode)
		if err != nil {
			if _, ok := err.(*wirevpn.AuthErr); ok {
				mainThread.RunOnMain(func() {
					c.SetLoginStatus(false)
					c.raiseNotif(err.Error())
				})
				return
			}
			mainThread.RunOnMain(func() {
				c.SetErrorPopupText(apiErr(err))
				c.SetConnectBusy(false)
			})
			return
		}
		cfg, err := config.OpenString(quick)
		if err != nil {
			mainThread.RunOnMain(func() {
				c.SetErrorPopupText(apiErr(err))
				c.SetConnectBusy(false)
			})
			return
		}
		priv, _ := stn.Keys()
		cfg.Interface.PrivateKey = priv
		newQuick, err := cfg.Bytes()
		if err != nil {
			mainThread.RunOnMain(func() {
				c.SetErrorPopupText(apiErr(err))
				c.SetConnectBusy(false)
			})
			return
		}
		service.Connect(string(newQuick))
	}()
}

// [API] [QML] Disconnect from VPN
func (c *customWindow) disconnect() {
	c.SetConnectBusy(true)
	go service.Disconnect()
}

// Returns a random background photo
func (c *customWindow) getRandomBg() string {
	rand.Seed(time.Now().UnixNano())
	i := rand.Intn(5) + 1
	return fmt.Sprintf("images/bg%d.png", i)
}

// Returns the account pass
func (c *customWindow) getAccountPass() string {
	return stn.AccountPass()
}

// Returns true if user is already logged in
func (c *customWindow) isLoggedIn() bool {
	if stn.Token() != "" && stn.AccountPass() != "" {
		return true
	}
	return false
}
