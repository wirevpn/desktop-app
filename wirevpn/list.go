package main

import (
	"github.com/therecipe/qt/core"
)

const (
	countryFlag = int(core.Qt__UserRole) + 1<<iota
	countryName
	countryCode
	signalStrength
)

type listItem struct {
	core.QObject

	_ string `property:"code"`
	_ string `property:"flag"`
	_ string `property:"name"`
	_ string `property:"strength"`
}

type customListModel struct {
	core.QAbstractListModel

	_ []*listItem              `property:"items"`
	_ map[int]*core.QByteArray `property:"roles"`
	_ func()                   `constructor:"init"`
	_ func(int) string         `slot:"getCurrentCode,auto"`
}

func (m *customListModel) init() {
	m.SetRoles(map[int]*core.QByteArray{
		countryCode:    core.NewQByteArray2("code", len("code")),
		countryFlag:    core.NewQByteArray2("flag", len("flag")),
		countryName:    core.NewQByteArray2("name", len("name")),
		signalStrength: core.NewQByteArray2("strength", len("strength")),
	})
	m.ConnectRowCount(m.rowCount)
	m.ConnectData(m.data)
	m.ConnectRoleNames(m.roleNames)
}

func (m *customListModel) data(index *core.QModelIndex, role int) *core.QVariant {
	if !index.IsValid() {
		return core.NewQVariant()
	}
	if index.Row() >= len(m.Items()) {
		return core.NewQVariant()
	}
	item := m.Items()[index.Row()]
	switch role {

	case countryCode:
		return core.NewQVariant14(item.Code())

	case countryFlag:
		return core.NewQVariant14(item.Flag())

	case countryName:
		return core.NewQVariant14(item.Name())

	case signalStrength:
		return core.NewQVariant14(item.Strength())

	default:
		return core.NewQVariant()
	}
}

func (m *customListModel) rowCount(index *core.QModelIndex) int {
	return len(m.Items())
}

func (m *customListModel) columnCount(index *core.QModelIndex) int {
	return 1
}

func (m *customListModel) roleNames() map[int]*core.QByteArray {
	return m.Roles()
}

func (m *customListModel) getCurrentCode(index int) string {
	return m.Items()[index].Code()
}
