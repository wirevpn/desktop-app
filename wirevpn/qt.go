package main

import (
	"image/color"
	"os"

	qrcode "github.com/skip2/go-qrcode"
	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/qml"
	"github.com/therecipe/qt/quick"
	"github.com/therecipe/qt/widgets"
)

func setupQT() *qml.QQmlComponent {
	core.QCoreApplication_SetAttribute(core.Qt__AA_EnableHighDpiScaling, false)

	mainApp = widgets.NewQApplication(len(os.Args), os.Args)
	setupAssets(core.QCoreApplication_ApplicationDirPath())
	mainApp.SetWindowIcon(gui.NewQIcon5(imgWinIcon))

	gui.QFontDatabase_AddApplicationFont(fontInconsolata)
	gui.QFontDatabase_AddApplicationFont(fontOpenSans)
	gui.QFontDatabase_AddApplicationFont(fontOpenSansBold)

	// QR provider
	img := quick.NewQQuickImageProvider(0, 0)
	img.ConnectRequestImage(func(id string, size *core.QSize, requestedSize *core.QSize) *gui.QImage {
		if id == "" {
			return gui.NewQPixmap5(imgQrPlaceholder, "", 0).ToImage()
		}
		qr, _ := qrcode.New(id, qrcode.Medium)
		qr.BackgroundColor = color.RGBA{236, 233, 230, 255}
		qr.ForegroundColor = color.RGBA{75, 100, 119, 255}
		png, _ := qr.PNG(16)
		pix := gui.NewQPixmap()
		pix.LoadFromData(png, uint(len(png)), "", 0)
		return pix.ToImage()
	})

	countryList = NewCustomListModel(nil)

	qmlApp := qml.NewQQmlEngine(nil)
	qmlApp.AddImageProvider("qrcode", img)
	qmlApp.RootContext().SetContextProperty("CustomListModel", countryList)

	return qml.NewQQmlComponent5(qmlApp, core.NewQUrl3("qrc:/qml/MainWindow.qml", 0), nil)
}
