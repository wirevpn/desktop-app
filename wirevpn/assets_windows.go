package main

import (
	"path/filepath"
)

func setupAssets(exePath string) {
	imgWinIcon = filepath.Join(exePath, "images", "sysIcon.ico")
	imgSysIcon = filepath.Join(exePath, "images", "sysIcon.ico")
	imgSysIconGreen = filepath.Join(exePath, "images", "sysIconGreen.ico")
	imgQrPlaceholder = filepath.Join(exePath, "images", "ph.png")
	fontInconsolata = filepath.Join(exePath, "fonts", "Inconsolata.otf")
	fontOpenSansBold = filepath.Join(exePath, "fonts", "OpenSans-Bold.ttf")
	fontOpenSans = filepath.Join(exePath, "fonts", "OpenSans-Regular.ttf")
}
