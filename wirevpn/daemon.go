package main

type daemonAPI interface {
	Establish() error
	Connect(config string)
	Disconnect()
	Close()
}
