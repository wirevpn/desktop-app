package settings

import (
	"encoding/json"
	"sync"

	"github.com/99designs/keyring"
	"gitlab.com/wirevpn/desktop-app/wirevpn/libs/paths"
)

type cdata struct {
	AccountPass string `json:"account_pass"`
	DeviceID    string `json:"device_id"`
	PrivateKey  string `json:"private_key"`
	PublicKey   string `json:"public_key"`
	Token       string `json:"token"`
	LastCountry string `json:"last_country"`
}

// Settings holds data of client settings
type Settings struct {
	data    cdata
	keyring keyring.Keyring
	appName string
	lock    sync.RWMutex
}

// NewSettings creates or opens the app settings from keyring
// Most of the data is sensitive we keep everything there for ease of retreiving
func NewSettings(appName string) (*Settings, error) {
	kr, _ := keyring.Open(keyring.Config{
		ServiceName: appName,
	})
	secret, err := kr.Get("config")
	if len(secret.Data) == 0 || err != nil {
		return &Settings{
			data:    cdata{AccountPass: paths.GetString(appName)},
			appName: appName,
			keyring: kr,
		}, nil
	}

	var ret Settings
	if err := json.Unmarshal(secret.Data, &ret.data); err != nil {
		return nil, err
	}
	ret.keyring = kr
	ret.appName = appName
	return &ret, nil
}

func (c *Settings) save() error {
	cj, err := json.Marshal(&c.data)
	if err != nil {
		return err
	}
	err = c.keyring.Set(keyring.Item{
		Key:  "config",
		Data: cj,
	})
	if err != nil {
		return err
	}
	return nil
}

// SetAccountPass sets accountPass and saves file
func (c *Settings) SetAccountPass(accountPass string) error {
	c.lock.Lock()
	defer c.lock.Unlock()
	c.data.AccountPass = accountPass
	paths.SaveString(c.appName, accountPass)
	return c.save()
}

// SetDeviceID sets deviceID and saves file
func (c *Settings) SetDeviceID(deviceID string) error {
	c.lock.Lock()
	defer c.lock.Unlock()
	c.data.DeviceID = deviceID
	return c.save()
}

// SetLastCountry sets the last connected country
func (c *Settings) SetLastCountry(country string) error {
	c.lock.Lock()
	defer c.lock.Unlock()
	c.data.LastCountry = country
	return c.save()
}

// SetToken sets token and saves file
func (c *Settings) SetToken(token string) error {
	c.lock.Lock()
	defer c.lock.Unlock()
	c.data.Token = token
	return c.save()
}

// SetKeyPair sets public and private key and saves file
func (c *Settings) SetKeyPair(privateKey, publicKey string) error {
	c.lock.Lock()
	defer c.lock.Unlock()
	c.data.PrivateKey = privateKey
	c.data.PublicKey = publicKey
	return c.save()
}

// AccountPass returns account pass
func (c *Settings) AccountPass() string {
	c.lock.RLock()
	defer c.lock.RUnlock()
	return c.data.AccountPass
}

// DeviceID returns device ID
func (c *Settings) DeviceID() string {
	c.lock.RLock()
	defer c.lock.RUnlock()
	return c.data.DeviceID
}

// LastCountry returns the last connected country
func (c *Settings) LastCountry() string {
	c.lock.RLock()
	defer c.lock.RUnlock()
	return c.data.LastCountry
}

// Token returns JWT token
func (c *Settings) Token() string {
	c.lock.RLock()
	defer c.lock.RUnlock()
	return c.data.Token
}

// Keys return crypto client keys
func (c *Settings) Keys() (string, string) {
	c.lock.RLock()
	defer c.lock.RUnlock()
	return c.data.PrivateKey, c.data.PublicKey
}
