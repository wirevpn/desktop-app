package settings

import "testing"

func TestConfig(t *testing.T) {
	settings, err := NewSettings("test_app")
	if err != nil {
		t.Error(err)
	}

	err = settings.SetAccountPass("random")
	if err != nil {
		t.Error(err)
	}

	err = settings.SetDeviceID("uuid")
	if err != nil {
		t.Error(err)
	}

	err = settings.SetToken("token")
	if err != nil {
		t.Error(err)
	}

	if settings.AccountPass() != "random" {
		t.Error()
	}

	if settings.Token() != "token" {
		t.Error()
	}

	if settings.DeviceID() != "uuid" {
		t.Error()
	}

	if a, b := settings.Keys(); a != "" || b != "" {
		t.Error()
	}
}
