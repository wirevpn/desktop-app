package wirevpn

import (
	"log"
	"testing"
)

func TestAPI(t *testing.T) {
	c := NewClient("https://testapp.wirevpn.net/v3")
	token, err := c.Login("LFHT2S940EBG", "SOME_DEVICE_ID", "SOME_KEY")
	if err != nil {
		t.Error(err)
	}

	c.SetToken(token)

	_, err = c.UserInfo()
	if err != nil {
		t.Error(err)
	}

	err = c.SaveKey("MY_KEY")
	if err != nil {
		t.Error(err)
	}

	_, err = c.CountryList()
	if err != nil {
		t.Error(err)
	}

	d, err := c.Connect("usa")
	if err != nil {
		log.Println(d)
		t.Error(err)
	}
}
