package wirevpn

import (
	"encoding/base64"
	"encoding/json"
	"net/http"
)

// Connect connects to a server from the given country
func (c *Client) Connect(countryCode string) (string, error) {
	var input struct {
		CountryCode string `json:"country_code"`
	}
	var result struct {
		Data string `json:"data"`
	}

	input.CountryCode = countryCode
	r, err := c.fetch(http.MethodPost, "/connections", &input)
	if err != nil {
		return "", err
	}

	err = json.Unmarshal(r, &result)
	if err != nil {
		return "", err
	}

	confDecoded, err := base64.StdEncoding.DecodeString(result.Data)
	if err != nil {
		return "", err
	}

	return string(confDecoded), nil
}
