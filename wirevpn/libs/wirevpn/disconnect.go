package wirevpn

import (
	"net/http"
)

// Disconnect deletes the session of a client
func (c *Client) Disconnect() error {
	_, err := c.fetch(http.MethodDelete, "/connections", nil)
	if err != nil {
		return err
	}
	return nil
}
