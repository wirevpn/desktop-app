package wirevpn

import (
	"encoding/json"
	"net/http"
)

// User holds information about a user
type User struct {
	AccountID string `json:"account_id"`
	PayID     string `json:"pay_id"`
	DaysLeft  int    `json:"days_left"`
	Plan      string `json:"plan"`
	ClientVer string `json:"client_ver"`
}

// UserInfo gets user's info with the JWT token
func (c *Client) UserInfo() (*User, error) {
	result := struct {
		Data *User
	}{
		Data: &User{},
	}

	r, err := c.fetch(http.MethodGet, "/users", nil)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(r, &result)
	if err != nil {
		return nil, err
	}

	return result.Data, nil
}
