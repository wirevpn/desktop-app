package wirevpn

import (
	"encoding/json"
	"net/http"
)

// Login creates a new session and returns the JWT token
func (c *Client) Login(accountID, deviceID, publicKey string) (string, error) {
	input := struct {
		AccountID string `json:"account_id"`
		DeviceID  string `json:"device_id"`
		PublicKey string `json:"public_key"`
	}{
		AccountID: accountID,
		DeviceID:  deviceID,
		PublicKey: publicKey,
	}

	result := struct {
		Data struct {
			Token string `json:"jwt_token"`
		} `json:"data"`
	}{}

	r, err := c.fetch(http.MethodPost, "/sessions", &input)
	if err != nil {
		return "", err
	}

	err = json.Unmarshal(r, &result)
	if err != nil {
		return "", err
	}

	return result.Data.Token, nil
}
