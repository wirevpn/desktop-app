package wirevpn

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

const (
	timeout     = 3 * time.Second
	idleTimeout = 6 * time.Second
)

// AuthErr happens when JWT is expired
type AuthErr struct{}

func (a *AuthErr) Error() string {
	return "Authorization expired. Please re-login."
}

// Client holds information about a WireVPN client
type Client struct {
	client   *http.Client
	token    string
	endpoint string
}

// NewClient creates a new WireVPN client
func NewClient(endpoint string) *Client {
	tr := &http.Transport{IdleConnTimeout: idleTimeout}
	if os.Getenv("WV_TEST") == "LOCAL_TEST" {
		tr.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	}

	return &Client{
		client: &http.Client{
			Timeout:   timeout,
			Transport: tr,
		},
		endpoint: endpoint,
	}
}

// SetToken sets JWT token
func (c *Client) SetToken(token string) {
	c.token = token
}

func (c *Client) fetch(method, path string, body interface{}) ([]byte, error) {
	var bodyReader *bytes.Reader
	if body != nil {
		data, err := json.Marshal(body)
		if err != nil {
			return nil, err
		}
		bodyReader = bytes.NewReader(data)
	} else {
		bodyReader = bytes.NewReader([]byte(""))
	}

	fullPath := c.endpoint + path
	request, err := http.NewRequest(method, fullPath, bodyReader)
	if err != nil {
		return nil, err
	}
	request.Header.Add("Accept", "application/json")
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Authorization", "Bearer "+c.token)

	resp, err := c.client.Do(request)
	if err != nil {
		return nil, err
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	result := make(map[string]interface{})
	err = json.Unmarshal(respBody, &result)
	if err != nil {
		return nil, fmt.Errorf("API server connection error")
	}

	if status, ok := result["status"]; ok {
		status := int(status.(float64))
		if status != http.StatusOK {
			if status == http.StatusUnauthorized {
				return nil, &AuthErr{}
			}
			if msg, ok := result["message"]; ok {
				return nil, fmt.Errorf(msg.(string))
			}
			return nil, fmt.Errorf("API server unknown error")
		}
	}

	return respBody, nil
}
