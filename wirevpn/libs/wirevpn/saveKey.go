package wirevpn

import (
	"net/http"
)

// SaveKey saves the public key of the client in server
func (c *Client) SaveKey(publicKey string) error {
	input := struct {
		PublicKey string `json:"public_key"`
	}{
		PublicKey: publicKey,
	}

	_, err := c.fetch(http.MethodPost, "/keys", &input)
	if err != nil {
		return err
	}

	return nil
}
