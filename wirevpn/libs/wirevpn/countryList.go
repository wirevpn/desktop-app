package wirevpn

import (
	"encoding/json"
	"net/http"
)

// Country holds information about a country
type Country struct {
	Code     string `json:"code"`
	Name     string `json:"name"`
	Strength int    `json:"strength"`
}

// CountryList gets up-to-date server list from server
func (c *Client) CountryList() ([]*Country, error) {
	var result struct {
		Data []*Country
	}

	r, err := c.fetch(http.MethodGet, "/countries", nil)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(r, &result)
	if err != nil {
		return nil, err
	}

	return result.Data, nil
}
