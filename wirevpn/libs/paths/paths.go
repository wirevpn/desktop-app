package paths

import (
	"io/ioutil"
	"os"
	"path/filepath"
)

// SaveString saves a string to the given config file, replaces everything
func SaveString(appName, str string) error {
	cfg := configPath(appName)
	err := os.MkdirAll(cfg, 0770)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(filepath.Join(cfg, appName+".conf"), []byte(str), 0660)
	if err != nil {
		return err
	}
	return nil
}

// GetString retrieves a string from given config file
func GetString(appName string) string {
	file := filepath.Join(configPath(appName), appName+".conf")
	str, _ := ioutil.ReadFile(file)
	return string(str)
}
