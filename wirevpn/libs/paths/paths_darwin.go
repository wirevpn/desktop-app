package paths

import "os"

func configPath(folder string) string {
	return os.Getenv("HOME") + "/Library/Application Support/" + folder
}
