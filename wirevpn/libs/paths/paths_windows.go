package paths

import "os"

func configPath(folder string) string {
	return os.Getenv("APPDATA") + "\\" + folder
}
