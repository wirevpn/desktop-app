// +build freebsd openbsd linux

package paths

import "os"

func configPath(folder string) string {
	if os.Getenv("XDG_CONFIG_HOME") != "" {
		return os.Getenv("XDG_CONFIG_HOME") + "/" + folder
	} else {
		return os.Getenv("HOME") + ".config/" + folder
	}
}
