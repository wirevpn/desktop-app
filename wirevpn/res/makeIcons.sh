#!/bin/bash

# make sure inkscape, magick, libicns, and mingw-w64 (for windres) is installed

for x in 16 32 48 128 256 512 ; do inkscape --export-png sysIcon_${x}x${x}.png -w ${x} sysIcon.svg ; done
magick sysIcon_*.png sysIcon.ico
magick sysIcon_16x16.png sysIcon_32x32.png sysIcon2.ico
png2icns sysIcon.icns sysIcon_*.png

for x in 16 32 48 128 256 512 ; do inkscape --export-png uninstIcon_${x}x${x}.png -w ${x} uninstIcon.svg ; done
magick uninstIcon_*.png uninstIcon.ico

for x in 16 32 ; do inkscape --export-png sysIconGreen_${x}x${x}.png -w ${x} sysIconGreen.svg ; done
magick sysIconGreen_*.png sysIconGreen.ico

# windows
x86_64-w64-mingw32-windres icon.rc -o ../icon_windows.syso
mv sysIcon2.ico ../windows/images/sysIcon.ico
mv sysIconGreen.ico ../windows/images/sysIconGreen.ico
mv sysIcon.ico ../../windows/setup.ico
mv uninstIcon.ico ../../windows/uninst.ico

# darwin
cp sysIcon_32x32.png ../darwin/Contents/Resources/sysIcon.png
cp sysIconGreen_32x32.png ../darwin/Contents/Resources/sysIconGreen.png
mv sysIcon.icns ../darwin/Contents/Resources/winIcon.icns

# linux
cp sysIcon_32x32.png ../linux/images/sysIcon.png
cp sysIconGreen_32x32.png ../linux/images/sysIconGreen.png
mv sysIcon_128x128.png ../linux/images/winIcon.png

# cleanup
rm uninstIcon_*.png
rm sysIcon_*.png
rm sysIconGreen_*.png